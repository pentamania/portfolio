window.ElementJumper = (function() {
  var _ship, _targets;

  _ship = null;

  _targets = null;

  function ElementJumper(shipElement, targetElements, index) {
    _ship = shipElement;
    _targets = targetElements;
    _ship.style.position = "absolute";
    if ((index != null)) {
      this.jump(index);
    }
  }

  ElementJumper.prototype.jump = function(index) {
    var _shipW, _tgt, _tgtCenterX, _tgtRect;
    if (index == null) {
      index = 0;
    }
    _tgt = _targets[index];
    if (!_tgt) {
      console.warn("target not defined");
      return;
    }
    _tgtRect = _tgt.getBoundingClientRect();
    _tgtCenterX = _tgtRect.left + _tgtRect.width / 2;
    _shipW = _ship.getBoundingClientRect().width;
    return _ship.style.left = _tgtCenterX - _shipW / 2 + "px";
  };

  return ElementJumper;

})();


/*
cssクラス初期名は
statusDisplay-label
statusDisplay-value
statusDisplay-value--tag
 */

window.StatusDisplay = (function() {
  var _data, _labels, _maxIndex, _owner;

  _owner = null;

  _labels = null;

  _data = null;

  _maxIndex = null;

  function StatusDisplay(ownerElement, labels, data) {
    _owner = ownerElement;
    _labels = labels;
    _data = data;
    _maxIndex = _data.length - 1;
    this.setUp();
  }

  StatusDisplay.prototype.setUp = function() {
    _labels.forEach((function(_this) {
      return function(label) {
        var attrName, child, childHTML, grandchildHTML, labelName;
        attrName = label[0];
        labelName = label[1];
        childHTML = "<div class='statusDisplay-label' name=" + attrName + ">" + labelName + ": </div>";
        _owner.innerHTML += childHTML;
        child = document.getElementsByName(attrName);
        grandchildHTML = "<span class='statusDisplay-value'></span>";
        return child[0].innerHTML += grandchildHTML;
      };
    })(this));
    return this.update(0);
  };

  StatusDisplay.prototype.update = function(index) {
    if (index == null) {
      index = 0;
    }
    if (index < 0 || _maxIndex < index) {
      console.warn("undefined index");
      return;
    }
    return _labels.forEach(function(label) {
      var attrName, val, valElement, _valElement;
      attrName = label[0];
      _valElement = document.getElementsByName(attrName);
      valElement = _valElement[0].querySelector('.statusDisplay-value');
      valElement.innerHTML = "";
      val = _data[index][attrName];
      if (typeof val === 'object') {
        return val.forEach(function(tag) {
          return valElement.innerHTML += "<span class='statusDisplay-value--tag'>" + tag + "</span>";
        });
      } else {
        return valElement.innerHTML += val;
      }
    });
  };

  return StatusDisplay;

})();

(function() {
  var app, hexTabs;
  hexTabs = [
    {
      label: "DESIGN",
      contents: {
        "PHOTOSHOP(CS2)": "使用歴：５年以上、主に絵の制作・画像の調整・資料作成など",
        "ILLUSTRATOR(CS2)": "使用歴：１年程度、ロゴ制作・キャラクター画像制作などに使用"
      },
      refurl: "https://crowdworks.jp/public/employees/267705/proposal_products"
    }, {
      label: "PROGRAMMING",
      contents: {
        "使用言語": "JavaScript, PHP / 独学にて１年程度",
        "ゲームプログラミング": "HTML5Canvasゲーム制作、パズルゲーム制作",
        "JS library": "使用経験のあるライブラリ:　jQuery, enchant.js, tmlib.js, createJs",
        "ALTjs": "coffeeScript / 本サイトではcoffeeScriptを使用"
      }
    }, {
      label: "FRONTEND",
      contents: {
        "gulp.js": "当サイト構築に使用",
        "jade": "当サイト構築に使用",
        "LESS/SCSS": "同程度の理解度、最近はSCSSメイン。当サイトではSCSSを使用",
        "Angular.js": "本サイトの２ページ目で使用"
      }
    }
  ];
  app = angular.module('myApp', []);
  app.factory('presentationFactory', function() {
    return {
      show: function(index) {
        return console.log("helloooooooooo!!");
      }
    };
  });
  app.controller('ExhibitCtrl', function($scope, presentationFactory) {
    var factory;
    factory = presentationFactory;
    $scope.tabs = hexTabs;
    $scope.tabIndex = 0;
    $scope.getTabIndex = function(t) {
      return console.log(t);
    };
    $scope.greet = function() {
      return factory.show();
    };
    return $scope.showActiveDetail = function(t) {
      if (t.isActive === true) {
        t.isActive = false;
        return;
      }
      $scope.tabs.forEach(function(tab) {
        return tab.isActive = false;
      });
      t.isActive = true;
    };
  });
  app.directive('detail', function($parse) {
    return {
      restrict: 'EA',
      replace: true,
      template: "<button><i ng-transclude>{{value}}<i></button>",
      transclude: true,
      controller: function($scope, $element) {
        return this.something = "greats!!";
      },
      link: function(scope, element, attr, ctrl) {
        var value;
        return value = $parse(attr.detail)(scope);
      }
    };
  });
  return app.service('presentationService', function($window) {
    this.bar = 0;
    this.greeter = function(text) {
      console.log(text);
      return $window.alert(text);
    };
  });
})();


/*

Portfolio
* 1st page carousel setUp
*
* require jquery.js, jquery-owlcarousel.js, class.coffee
 */
(function() {
  var contentItems, currentPageIndex, enableScroll, owlAfterAction, pageNum, setUpCarousel, ship, siteStats, status, statusLabels;
  contentItems = ['WORKS', 'WATASHI', 'RENRAKU'];
  siteStats = [
    {
      siteName: "スタークリエイション",
      siteType: "企業サイト、ビビッド",
      skill: ["LESS", "PHP", "jQuery", "レスポンシブ", "IE8対応"],
      image: "./assets/images/workExp_starCreation.png",
      url: 'http://www.star-creation.net'
    }, {
      siteName: "pentamania",
      siteType: "クリエイティブ、ゲーム性",
      skill: ["enchant.js", "canvas-2D", "レスポンシブ", "物理演算"],
      image: "./assets/images/workExp_mySite.png",
      url: 'http://pentamania.web.fc2.com'
    }, {
      siteName: "ここ",
      siteType: "クリエイティブ、さわやか、ペライチ",
      skill: ["gulp.js", "jQuery", "jade", "coffeescript", "SCSS"],
      image: "./assets/images/koko.png",
      url: '#'
    }
  ];
  statusLabels = [[Object.keys(siteStats[0])[0], "名前"], [Object.keys(siteStats[0])[1], "サイトテーマ"], [Object.keys(siteStats[0])[2], "使用スキル"]];
  currentPageIndex = 0;
  enableScroll = true;
  pageNum = document.querySelectorAll('.page').length;
  ship = null;
  status = null;
  $(document).ready(function() {
    var owl, statusField, _ship, _stations;
    owl = $("#works-carousel");
    statusField = document.querySelector(".status-box");
    setUpCarousel(owl, siteStats);
    status = new StatusDisplay(statusField, statusLabels, siteStats);
    _ship = document.getElementById('indicator-ship');
    _stations = document.querySelectorAll('.indicator-element');
    ship = new ElementJumper(_ship, _stations, currentPageIndex);
    owl.owlCarousel({
      items: 3,
      itemsDesktop: [1199, 3],
      itemsTablet: false,
      itemsMobile: false,
      pagination: false,
      slideSpeed: 100,
      paginationSpeed: 200,
      navigation: true,
      navigationText: ["prev", "next"],
      rewindNav: false,
      itemsScaleUp: true,
      afterAction: owlAfterAction
    });
    return $(".veil").animate({
      top: "-2000px",
      opacity: 0
    }, 1200);
  });
  setUpCarousel = function(selector, stats) {
    stats.forEach(function(stat) {
      return $(selector).append("<div class='carousel-item'><a href=" + stat.url + " target='new'><img src=" + stat.image + " alt=" + stat.siteName + " /><a></div>");
    });
    $(selector).prepend("<div class='carousel-item dummy'></div>");
    return $(selector).append("<div class='carousel-item dummy'></div>");
  };
  owlAfterAction = function() {
    var addFocusClass, focusedItemIndex, imageURL;
    focusedItemIndex = this.owl.currentItem + 1;
    addFocusClass = function(jquerySelector) {
      var selector, _i, _len, _results;
      _results = [];
      for (_i = 0, _len = arguments.length; _i < _len; _i++) {
        selector = arguments[_i];
        _results.push(selector.each(function(index, element) {
          $(this).removeClass("focus");
          if (index === focusedItemIndex) {
            return $(this).addClass("focus");
          }
        }));
      }
      return _results;
    };
    addFocusClass($(".carousel-item"), $(".owl-item"));
    status.update(this.owl.currentItem);
    imageURL = siteStats[this.owl.currentItem].image;
    return $('#page1').css({
      'background-image': "url(" + imageURL + ")"
    });
  };
  $(window).on('scroll', function() {
    var currentPos, fullHeight, pageHeight, pageRect, pageTop;
    if (enableScroll !== true) {
      return;
    }
    currentPos = $(document).scrollTop();
    fullHeight = Math.max($(document).height(), $(window).height());
    pageRect = $(".page")[currentPageIndex].getBoundingClientRect();
    pageTop = pageRect.top;
    pageHeight = $(".page")[currentPageIndex].scrollHeight;
    console.log("current", currentPos);
    console.log("pageHeight: ", pageHeight);
    console.log("pageTop", pageTop);
    if (currentPos > pageTop + pageHeight * 0.5 && currentPageIndex < pageNum - 1) {
      currentPageIndex++;
      ship.jump(currentPageIndex);
    } else if (currentPos < pageTop) {
      currentPageIndex--;
      ship.jump(currentPageIndex);
    }
  });
  $(window).on('resize', function() {
    return ship.jump(currentPageIndex);
  });
  return $(".topNavLink__element").click(function() {
    var i, p, scrollDuration, thisClass;
    enableScroll = false;
    scrollDuration = 200;
    thisClass = this.className;
    i = $("." + thisClass).index(this);
    currentPageIndex = i;
    p = $(".page").eq(i).offset().top;
    $('html,body').animate({
      scrollTop: p
    }, {
      duration: scrollDuration,
      complete: function() {
        return enableScroll = true;
      }
    });
    return ship.jump(currentPageIndex);
  });
})();


/*
以下クラス化したため不要 後々消す
 */

//# sourceMappingURL=main.js.map