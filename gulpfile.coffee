# required library
gulp = require 'gulp'
plumber = require 'gulp-plumber'
browserSync = require 'browser-sync'
concat = require 'gulp-concat'

jade = require 'gulp-jade'
imagemin = require 'gulp-imagemin'
compass = require 'gulp-compass'
please = require 'gulp-pleeease'
coffee = require 'gulp-coffee'
uglify = require 'gulp-uglify'

sourcemaps = require 'gulp-sourcemaps'
streamqueue = require 'streamqueue'
vinylSourceStream = require 'vinyl-source-stream'
streamify = require 'gulp-streamify'
del = require 'del'
runSequence = require 'run-sequence'

#paths
$src = './src'
# $dev = './dev'
$dest = './dist' #TODO: 開発時は'./dev' productionは"./dist"
$tmp = './tmp'
$app = './app'
path =
    src:
        html: $src + '/'
        jade: $src + '/jade'
        coffee: $src + '/coffee'
        js: $src + '/js'
        jsLib: $src + '/js/libs'
        jsPlugin: $src + '/js/plugins'
        #less: $src + '/less'
        sass: $src + '/sass'
        cssLib: $src + '/css/libs'
        font: $src + '/fonts'
        image: $src + '/images/'
    tmp:
        js: $tmp + '/js'
        css: $tmp + '/css'
    dest:
        html: $dest + '/'
        js: $dest + '/assets/js'
        css: $dest + '/assets/css'
        image: $dest + '/assets/images'

gulp.task 'jade', ->
    gulp.src path.src.jade+'/*.jade'
    .pipe plumber()
    .pipe jade(pretty: true)
    .pipe gulp.dest $dest

###
coffeescriptのコンパイルとライブラリ群との結合は別々にやったほうがよさ気
http://stackoverflow.com/questions/26040358/gulp-different-pipe-collections-within-same-task-coffeescript-and-javascript
###

# jsコンパイル＋圧縮 その1: 順番に行う
gulp.task 'coffee', ->
    streamqueue objectMode: true,
        #class等事前に読み込む
        gulp.src path.src.coffee+'/_import/*.coffee'
        .pipe plumber({
            errorHandler:(err)->
                console.log(err)
                this.emit('end')
        })
        #.pipe sourcemaps.init #streamqueueとの組み合わせでは正常に出力されない？
        #    loadMaps: true
        .pipe coffee {bare:true}
        .pipe concat 'allClass.js'

        #メイン
        gulp.src path.src.coffee+'/*.coffee'
        .pipe plumber()
        #.pipe sourcemaps.init
        #    loadMaps: true
        .pipe coffee {bare:true}

    #.pipe sourcemaps.write '.',
    #    addComment: true
    #    sourceRoot: $src
    .pipe concat '_coffee.js'
    .pipe gulp.dest path.tmp.js

# オーソドックスなコンパイル＆ソース結合（＆sourcemap生成）
# ライブラリ・依存プラグインのみ
gulp.task 'concat-js-lib', ->
    gulp.src [
        path.src.jsLib+'/*.js'
        path.src.jsPlugin+'/*.js'
        ]
        .pipe sourcemaps.init
            loadMaps: true
        .pipe plumber()
        .pipe concat 'lib.js'
        .pipe uglify({preserveComments: 'some'}) #圧縮。ライセンス表記は残す
        .pipe sourcemaps.write '.',
            addComment: true
            sourceRoot: $src
        .pipe gulp.dest path.dest.js

# メイン処理
gulp.task 'concat-js-main',['coffee'], ->
    gulp.src [
        #path.src.jsLib+'/*.js'
        #path.src.jsPlugin+'/*.js'
        path.src.js+'/*.js' # pure js
        path.tmp.js+'/*.js' # coffee-compiled
    ]
    .pipe sourcemaps.init
        loadMaps: true
    .pipe plumber()
    .pipe concat 'main.js'
    #.pipe uglify() # 圧縮
    .pipe sourcemaps.write '.',
        addComment: true
        sourceRoot: $src
    #.pipe gulp.dest './testdest/'
    .pipe gulp.dest path.dest.js

### jsコンパイル＋圧縮 その2 ###
#コンパイルと結合（とsourcemapの出力）がいっぺんにできるが、plumberが仕事しなくなる（watchも途切れてしまう）; streamifyを挟めば解決する？->ダメでした
#gulp.task 'seq-js-compile', ->
#    streamqueue(
#        {objectMode: true}
#        gulp.src [
#            path.src.jsLib+'/*.js' #libraries
#            path.src.jsPlugin+'/*.js' #library-plugins
#            #path.src.js+'/*.js'
#            ]
#            .pipe plumber()
#            .pipe sourcemaps.init()
#        gulp.src path.src.coffee+'/*.coffee'
#            .pipe sourcemaps.init()
#            .pipe plumber()
#            .pipe coffee {bare:true}
#    )
#    .pipe concat 'all.js'
#    .pipe uglify({preserveComments: "some"}) #圧縮
#    #.pipe uglify() #米なし圧縮
#    #.pipe streamify() #NG
#    #.pipe vinylSourceStream() #NG
#    .pipe sourcemaps.write '.',
#            addComment: true
#            sourceRoot: $src
#    .pipe gulp.dest path.dest.js

# jsコンパイル＋圧縮 その３
#予め、ライブラリや生jsを全て結合したall.js的なフォルダを作る必要がある。
#gulp.task 'ajc2', ->
#    csResult = gulp.src [path.src.coffee+'/*.coffee']
#        .pipe plumber()
#        .pipe sourcemaps.init
#            loadMaps: true
#        .pipe coffee()
#
#    csResult
#        .pipe concat 'all.js'
#        #.pipe uglify()
#        .pipe sourcemaps.write()
#        .pipe gulp.dest './release/'

### compass(sass) compile & autopreix & minify ###
#pleeeaseについて => http://qiita.com/tokimari/items/8cb648e06c4db072e7aa
gulp.task 'compass', ->
    cssOutput = path.dest.css

    gulp.src path.src.sass+'/*.scss'
        .pipe plumber({
            errorHandler:(err)->
                console.log(err)
                this.emit('end')
        })
        .pipe compass(
            config_file: './config.rb'
            comments: true
            #style: 'compressed',#圧縮：デフォルトではやらない
            css: cssOutput #should be same value in config.rb?
            sass: path.src.sass #sass source directory; should be same value in config.rb?
            #images: $dest + '/assets/images' #iamge keeped directory; should be same value in config.rb
        )
        .pipe please(
            autoprefixer: {"browsers": ["last 4 versions", "Android 2.3"]}
            minifier: false #圧縮の有無
        )
        .pipe gulp.dest cssOutput

gulp.task 'concat-css-lib', ->
    gulp.src [
            path.src.cssLib+'/*.css'
            path.src.plugin+'/*.css' #library depended
        ]
        .pipe sourcemaps.init
            loadMaps: true
        .pipe plumber()
        .pipe concat 'lib.css'
        .pipe sourcemaps.write '.',
            addComment: true
            sourceRoot: $src
        .pipe please( # error on bootstrap
            #autoprefixer: {"browsers": ["last 4 versions", "Android 2.3"]}
            minifier: false #圧縮の有無
        )
        .pipe gulp.dest path.dest.css

gulp.task 'concat-css-main', ['compass'], ->
    gulp.src [
            #path.src.cssLib+'/*.css'
            #path.src.plugin+'/*.css' #library depended
            path.src.css+'/*.css'
            path.tmp.css+'/*.css' #pre-compiled
        ]
        .pipe sourcemaps.init
            loadMaps: true
        .pipe plumber()
        .pipe concat 'main.css'
        .pipe sourcemaps.write '.',
            addComment: true
            sourceRoot: $src
        .pipe please( #error on bootstrap
            autoprefixer: {"browsers": ["last 4 versions", "Android 2.3"]}
            minifier: false #圧縮の有無
        )
        .pipe gulp.dest path.dest.css

# image最適化
gulp.task 'imagemin', ->
    imageminOptions =
        optimizationLevel: 5

    gulp.src path.src.image+'/**/*.+(jpg|jpeg|png|gif|svg)'
        .pipe plumber()
        .pipe imagemin imageminOptions
        .pipe gulp.dest path.dest.image

#clean $destと中間フォルダの中身を消す
gulp.task 'clean', (cb)->
    del [$dest+"/**/*",$tmp+"/**/*"], cb
    console.log("cleaning...")

gulp.task 'clean:tmp', (cb)->
    del [$tmp+"/**/*"], cb
    console.log("cleaning tmp dir...")

buildTasks = [
    'jade'
    'concat-js-lib'
    #'seq-js-compile'
    #'compass'
    'concat-css-lib'
    'concat-js-main'
    'concat-css-main'
    'imagemin'
]

gulp.task 'build', buildTasks, ->
    console.log("building...")

gulp.task 'watch', ->
    gulp.watch $src+"/**/*", [
        'clean:tmp'
        'jade'
        #'coffee'
        #'compass'
        'concat-css-main'
        'concat-js-main'
        browserSync.reload
    ], (event)->
        #console.log(event) # 実行されない：　optsとcallback function は同時に指定できない？

    console.log("watching...")

#サーバー立ち上げ、 監視
gulp.task 'server',['watch'], ->
    browserSync(
        server:
            baseDir: $dest
            index: "index.html"
        browser: ["google chrome"] #複数指定可？
    )

# production
# 順番 clean -> build -> watch,server
# gulp.task 'default',['concat-js']
gulp.task 'default', (callback)->
    runSequence(
        'clean'
        'build'
        'server' #watch -> server
        callback
    )

# dev
# 初回のみ clean -> build (js/cssライブラリ・プラグインとメインスクリプトは分ける)
# 以降 watch (メインスクリプトのコンパイル結合)
gulp.task 'devbuilt', (callback)->
    runSequence(
        'concat'
        ''
        callback
    )
