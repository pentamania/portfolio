###

Portfolio
* 1st page carousel setUp
*
* require jquery.js, jquery-owlcarousel.js, class.coffee
###
do ->
  contentItems = [
    'WORKS'
    'WATASHI'
    'RENRAKU'
  ]

  #model?
  siteStats = [
    {
      siteName: "スタークリエイション"
      siteType: "企業サイト、ビビッド"
      skill: ["LESS", "PHP", "jQuery", "レスポンシブ", "IE8対応"]
      image: "./assets/images/workExp_starCreation.png"
      url: 'http://www.star-creation.net'
    },{
      siteName: "pentamania"
      siteType: "クリエイティブ、ゲーム性"
      skill: ["enchant.js", "canvas-2D", "レスポンシブ", "物理演算"]
      image: "./assets/images/workExp_mySite.png"
      url: 'http://pentamania.web.fc2.com'
    },{
      siteName: "ここ"
      siteType: "クリエイティブ、さわやか、ペライチ"
      skill: ["gulp.js", "jQuery", "jade", "coffeescript","SCSS"]
      image: "./assets/images/koko.png"
      url: '#'
    }
  ]

  #上記のsiteName, sityType skillにラベルを付ける
  statusLabels = [
    [Object.keys(siteStats[0])[0], "名前"]
    [Object.keys(siteStats[0])[1], "サイトテーマ"]
    [Object.keys(siteStats[0])[2], "使用スキル"]
  ]

  # statusLabels = [
  #     ['siteName', "名前"]
  #     ['siteType', "サイト属性"]
  #     ['skill', "使用技術"]
  # ]

  #global vars
  currentPageIndex = 0
  enableScroll = true
  pageNum = document.querySelectorAll('.page').length
  ship = null
  status = null

  $(document).ready ->
    owl = $("#works-carousel")
    statusField = document.querySelector(".status-box")
    setUpCarousel(owl, siteStats)


    #setUpStatus(status, siteStats, statusLabels)
    #updateStatus(0, siteStats, statusLabels) #中身を書く
    status = new StatusDisplay(statusField, statusLabels, siteStats)

    # _ship = document.querySelector('#indicator-ship')
    _ship = document.getElementById('indicator-ship')
    _stations = document.querySelectorAll('.indicator-element')

    ship = new ElementJumper(_ship, _stations, currentPageIndex)

    #set up owl-carousel
    owl.owlCarousel({
        items: 3,
        itemsDesktop: [1199,3],
        itemsTablet: false,
        itemsMobile: false,
        pagination: false,
        slideSpeed: 100,
        paginationSpeed: 200,
        navigation  : true,
        navigationText  : ["prev", "next"],
        rewindNav : false,
        itemsScaleUp : true,
        afterAction   : owlAfterAction
    })

    #unveil after load
    $(".veil").animate({
      top: "-2000px"
      opacity: 0
    },1200)


  #owlカルーセルの初期設定
  setUpCarousel = (selector, stats) ->
    stats.forEach (stat)->
      $(selector).append(
        "<div class='carousel-item'><a href="+stat.url+" target='new'><img src="+stat.image+" alt="+stat.siteName+" /><a></div>"
      )

    #前後に位置調整のためのダミーを配置する
    $(selector).prepend(
      "<div class='carousel-item dummy'></div>"
    )
    $(selector).append(
      "<div class='carousel-item dummy'></div>"
    )

  owlAfterAction = ()->
      focusedItemIndex = this.owl.currentItem + 1;

      #フォーカスされた要素にクラス付与
      #$(".owl-item").each (index, element)->
      #    $(this).removeClass("focus") #一旦外す
      #    if (index == focusedItemIndex)
      #        $(this).addClass("focus")

      #$(".carousel-item").each (index, element)->
      #    $(this).removeClass("focus") #一旦外す
      #    if (index == focusedItemIndex)
      #        $(this).addClass("focus")

      addFocusClass = (jquerySelector) ->
        for selector in arguments
          selector.each (index, element)->
            $(this).removeClass("focus") #一旦外す
            if (index == focusedItemIndex)
              $(this).addClass("focus")

      addFocusClass $(".carousel-item"),$(".owl-item")

      #workサイト情報を書き換え
      status.update(this.owl.currentItem)
      #updateStatus(this.owl.currentItem)

      #background change
      imageURL = siteStats[this.owl.currentItem].image
      $('#page1').css({
        'background-image': "url("+imageURL+")"
      })

  #マウスホイールイベント
  # mousewheelEvent = `'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll'`
  # $(document).on mousewheelEvent,(e)->
  #     e.preventDefault()
  #     windowHeight = window.innerHeight
  #     pageHeight = $(".page")[currentPageIndex].scrollHeight
  #     # console.log $(".page")[currentPageIndex]
  #     # console.log pageHeight
  #     #windowHeight =  #pageの絶対height値を取る
  #     scrollDuration = 200
  #     delta = `e.originalEvent.deltaY ? -(e.originalEvent.deltaY) : e.originalEvent.wheelDelta ? e.originalEvent.wheelDelta : -(e.originalEvent.detail)`
  #
  #     if (enableScroll)
  #       enableScroll = false;
  #
  #       if (delta < 0)
  #         # マウスホイールを下にスクロール
  #         console.log "scroll down"
  #         if (currentPageIndex < pageNum-1) then currentPageIndex += 1
  #
  #         #ページインジケータの操作
  #         # selectorJump("#indicator-ship", ".indicator-element", currentPageIndex)
  #         ship.jump(currentPageIndex)
  #         # スクロールを固定移動
  #         currentPos = $(this).scrollTop()
  #         $('html,body').animate({scrollTop: currentPos + pageHeight},
  #           duration: scrollDuration,
  #           complete:()->
  #             enableScroll = true
  #         )
  #       else
  #         # マウスホイールを上にスクロール
  #         console.log "scroll up"
  #         if (currentPageIndex > 0) then currentPageIndex -= 1
  #         # selectorJump("#indicator-ship", ".indicator-element", currentPageIndex)
  #         ship.jump(currentPageIndex)
  #
  #         currentPos = $(this).scrollTop()
  #         $('html,body').animate({scrollTop: currentPos - pageHeight},
  #           duration: scrollDuration,
  #           complete:()->
  #             enableScroll = true
  #         )

          # 最初のページではtopnavを全表示？
          # if (currentPageIndex == 0)

  # スクロールイベント for iOs Android
  # 全スクロール量を計測 -> 一定量で
  $(window).on 'scroll', ()->
    if (enableScroll isnt true) then return

    currentPos = $(document).scrollTop()
    fullHeight = Math.max($(document).height(), $(window).height())
    pageRect = $(".page")[currentPageIndex].getBoundingClientRect()
    pageTop = pageRect.top
    # pageTop = $(".page")[currentPageIndex].offsetTop
    pageHeight = $(".page")[currentPageIndex].scrollHeight

    console.log "current", currentPos
    # console.log "pageHeight: ", pageHeight
    # console.log  "currentPageIndex: ", currentPageIndex
    console.log "pageTop", pageTop

    if (currentPos > pageTop + pageHeight*0.5 and currentPageIndex < pageNum-1)
    # if (currentPos > pageTop + pageHeight*0.75 and currentPageIndex < pageNum-1)
        currentPageIndex++
        ship.jump(currentPageIndex)
    else if (currentPos < pageTop)
        currentPageIndex--
        ship.jump(currentPageIndex)

    return

  $(window).on 'resize', ()->
    #e.preventDefault()
    #windowHeight = window.innerHeight
    #bodyHeight = Math.max.apply( null, [document.body.clientHeight , document.body.scrollHeight, document.documentElement.scrollHeight, document.documentElement.clientHeight] );
    #pageHeight = $(".page")[currentPageIndex].scrollHeight
    #console.log windowHeight+" "+pageHeight

    # selectorJump("#indicator-ship", ".indicator-element", currentPageIndex)
    ship.jump(currentPageIndex)

  #top nav click event
  $(".topNavLink__element").click ()->
    enableScroll = false
    scrollDuration = 200
    thisClass = this.className

    i = $("."+thisClass).index this
    currentPageIndex = i
    p = $(".page").eq(i).offset().top
    $('html,body').animate(
        {scrollTop: p},
        duration: scrollDuration,
        complete:()->
            enableScroll = true
        )

    ship.jump(currentPageIndex)

###
以下クラス化したため不要 後々消す
###
  #ある要素上をジャンプする (No jQuery, ie8+)
  # selectorJump = (moverSelector, targetSelector, index) ->
  #     targets = document.querySelectorAll(targetSelector)
  #     _ship = document.querySelector(moverSelector)
  #     if _ship then @ship = _ship else
  #       console.warn "selectorJump: mover undefined"
  #       return
  #     @index = if (index) then index else 0
  #
  #     tgt = targets[@index]
  #     if !tgt
  #       console.warn "selectorJump: target undefined"
  #       return
  #     tgtRect = tgt.getBoundingClientRect()
  #     tgtCenterX = tgtRect.left + tgtRect.width/2
  #
  #     shipW = @ship.getBoundingClientRect().width
  #     @ship.style.left = tgtCenterX - shipW/2 + "px" #要素の中心に修正
  #     #ship.style.top = tgtRect.height/2 +"px"
  #
  # #ステータス表示初期化
  # setUpStatus = (ownerSelector, model=siteStats, labels=statusLabels) ->
  #   @owner = ownerSelector
  #   labels.forEach (label)=>
  #     attrName = label[0] #タグのname属性
  #     labelName = label[1] #表示文字列
  #     childHTML = "<li class='status-label' name="+attrName+">" +labelName+ ": </span></li>"
  #     @owner.innerHTML += childHTML
  #
  #     child = document.getElementsByName attrName #array
  #     grandchildHTML = "<span class='status-value'></span>"
  #     child[0].innerHTML += grandchildHTML
  #     #$('li[name='+attrName+']').append("<span class='status-value'></span>");
  #
  #
  # #サイト情報を書き換え、更新
  # updateStatus = (index=0, model=siteStats, labels=statusLabels) ->
  #   labels.forEach (label)->
  #     attrName = label[0] #タグのname属性　stats内のキー名はこれと一致していないといけない
  #     val = model[index][attrName]
  #     parent = $('li[name='+attrName+'] .status-value')
  #
  #     parent.empty()
  #     if (typeof val == 'object')
  #       val.forEach (tagName)->
  #         parent.append("<span class='status-value-tag'>"+tagName+"</span>")
  #     else
  #       parent.append(val)
