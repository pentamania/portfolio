do ->
  hexTabs = [
    {
      label: "DESIGN"
      contents:
        "PHOTOSHOP(CS2)": "使用歴：５年以上、主に絵の制作・画像の調整・資料作成など"
        "ILLUSTRATOR(CS2)": "使用歴：１年程度、ロゴ制作・キャラクター画像制作などに使用"
      refurl: "https://crowdworks.jp/public/employees/267705/proposal_products"
    }
    {
      label: "PROGRAMMING"
      contents:
        "使用言語":"JavaScript, PHP / 独学にて１年程度"
        "ゲームプログラミング": "HTML5Canvasゲーム制作、パズルゲーム制作"
        "JS library": "使用経験のあるライブラリ:　jQuery, enchant.js, tmlib.js, createJs"
        "ALTjs": "coffeeScript / 本サイトではcoffeeScriptを使用"
        # "SKILL TEST": "paizaランクB"
        # "フレームワーク": "wordpress, fuelPHP"
    #   refurl: "https://paiza.jp/challenges/info"
    }
    {
      label: "FRONTEND"
      contents:
        "gulp.js": "当サイト構築に使用"
        "jade": "当サイト構築に使用"
        "LESS/SCSS": "同程度の理解度、最近はSCSSメイン。当サイトではSCSSを使用"
        "Angular.js": "本サイトの２ページ目で使用"
    }
  ]

  app = angular.module('myApp', [])

　#test用
  app.factory 'presentationFactory', ()->
    return {
      #表示フラグをアクティブにし、列挙する内容を変更
      show: (index)->
        console.log "helloooooooooo!!"
    }

  app.controller 'ExhibitCtrl', ($scope, presentationFactory)->
    factory = presentationFactory
    $scope.tabs = hexTabs
    $scope.tabIndex = 0
    # $scope.animate = true
    # $scope.isActive = true

    $scope.getTabIndex = (t)->
      return console.log(t)

    $scope.greet = ()->
      # presentationService.greet($scope.name)
      factory.show()

    $scope.showActiveDetail = (t)->
      if t.isActive == true
        t.isActive = false
        return
      $scope.tabs.forEach (tab)->
        tab.isActive = false

      t.isActive = true
      return

  app.directive('detail', ($parse)->
    return {
      restrict: 'EA'
      replace: true
      template: "<button><i ng-transclude>{{value}}<i></button>"
      #templateUrl: 'partials/detailModal.html'
      transclude: true
      controller: ($scope, $element)->
        this.something = "greats!!"

      link: (scope, element, attr, ctrl)->
        #console.log element #templateの内容
        value = $parse(attr.detail)(scope) # object: 受け取った変数をパース
        #console.log scope

        #element.detail({
        #  title: value
        #})
    }
  )

  app.service 'presentationService', ($window)->
    @bar = 0
    @greeter = (text)->
      console.log text
      $window.alert(text)
    return
