
# 要素上ををジャンプするクラス版
class window.ElementJumper
  _ship = null
  _targets = null
  constructor: (shipElement, targetElements, index)->
    _ship = shipElement
    _targets = targetElements #array or object

    _ship.style.position = "absolute"
    if (index?) then @jump(index)

  jump: (index=0)->
    _tgt = _targets[index]
    # if (!_tgt instanceof Element)
    if (!_tgt)
      console.warn "target not defined"
      return
    _tgtRect = _tgt.getBoundingClientRect()
    _tgtCenterX = _tgtRect.left + _tgtRect.width/2

    _shipW = _ship.getBoundingClientRect().width
    _ship.style.left = _tgtCenterX - _shipW/2 + "px" #要素の中心に修正

# status設定クラス
###
cssクラス初期名は
statusDisplay-label
statusDisplay-value
statusDisplay-value--tag
###
class window.StatusDisplay
  _owner = null
  _labels = null
  _data = null
  _maxIndex = null

  constructor: (ownerElement, labels, data)->
    _owner = ownerElement
    _labels = labels
    _data = data

    _maxIndex = _data.length-1

    @setUp()

  setUp: ()->
    _labels.forEach (label)=>
      attrName = label[0] # タグのname属性
      labelName = label[1] # 表示文字列
      childHTML = "<div class='statusDisplay-label' name="+attrName+">" +labelName+ ": </div>"
      _owner.innerHTML += childHTML

      child = document.getElementsByName attrName
      grandchildHTML = "<span class='statusDisplay-value'></span>"
      child[0].innerHTML += grandchildHTML

    @update(0)

  #modelのデータを挿入
  update: (index=0)->
    if ( index < 0 || _maxIndex < index)
      console.warn("undefined index")
      return

    _labels.forEach (label)->
      attrName = label[0] #タグのname属性　model内のキー名はこれと一致していないといけない
      _valElement = document.getElementsByName attrName #nodelist
      valElement = _valElement[0].querySelector('.statusDisplay-value') #htmlElement

      valElement.innerHTML = ""
      val = _data[index][attrName]

      if (typeof val == 'object')
        val.forEach (tag)->
          valElement.innerHTML += "<span class='statusDisplay-value--tag'>"+tag+"</span>"
      else
        valElement.innerHTML += val
